package me.Mr_Puppy_wuppy.ServerRules.listener;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;

public class BedListener implements Listener {
	
	@EventHandler
	public void onBedEnter(PlayerBedEnterEvent event) {

		// Check event status
		if(event.isCancelled() || event.getBedEnterResult() != PlayerBedEnterEvent.BedEnterResult.OK) {
			return;
		}

		Player player = event.getPlayer();
		
		World world = player.getWorld();
		
		world.setTime(1000);
		if (world.hasStorm()) {
			world.setWeatherDuration(1);
		}
		player.sendMessage("You entered the bed");
	}

}
